(defgroup c11 nil
  "C11 major mode."
  :group 'languages
  :prefix "c11-")

(defcustom c11-indent-offset 2
  "Ammount of spaces to use for indentation."
  :group 'c11
  :type 'integer
  :safe #'integerp)

(defcustom c11-comment-start "//"
  "C11 single line comment starter."
  :group 'c11
  :type 'string)

(defcustom c11-uncuddled-else t
  "Wheter we should uncuddle `else' statements.

For example if the value of `c11-uncluddled-else' evaluates to
`T' then all `else/else i' statements will indented this way:

      }
      else if (expr) {
        ...
      }
      else {
        ...
      }

Instead of:

      } else if (expr) {
        ...
      } else (expr) {
        ...
      }"
  :group 'c11
  :type 'boolean)

(defcustom c11-switch-case-indent-offset 0
  "Amount of space a`case' statement should be indented
relatively to its `switch' statement."
  :group 'c11
  :type 'integer)

(defcustom c11-case-break-indent-offset 2
  "Amount of space a `break' statement should be indented relatively
to its `case' label."
  :group 'c11
  :type 'integer)

(defcustom c11-offset-between-function-name-and-arglist 1
  "Defines the amount of space between a function name and its arglist,
if the value is 1 then we would have such indentation on function definiton:

       void shutdown (int module_id);
                    ^
and on function invocation:

       shutdown (AUDIO_MODULE);
               ^"
  :group 'c11
  :type 'integer)

(defcustom c11-function-brace-follows-statement t
  "Defines wheter a brace should follow a function definition or simply
colapse in the next line.

For instance if `c11-function-brace-follows-statement' has a value that
evaluates to `t' then we will have the following indentation:

        widget_t* create_widget() {
          ...                    ^
        }

instead of:

        widget_t* create_widget()
        {
          ...
        }"
  :group 'c11
  :type 'boolean)

(defcustom c11-function-brace-offset 1
  "When `c11-function-brace-follows-statement' is non nil it specifies
the amount of space between a function definition symbol and de opening
curly braces, for instance if it has a value of 0 we will have the
following indentation:

        long integer_dup(long num){
         (...)                   ^
        }

note the lack of space between a paren and the opening curly brace.

In case of `c11-function-brace-follows-statement' has a nil value then it
species de amount of space bewteen de openning curly brace and the left
fringe, for instance if its value is 2 then we will have the
following indentation:

        void swap_integer(long* first, long* second)
          {
        ^^
          }"
  :group 'c11
  :type 'boolean)

(defcustom c11-offset-between-block-and-paren 1
  "Defines the amount of space between a block construct
such as if/elsif/for/while."
  :group 'c11
  :type 'integer)

(defcustom c11-brace-follows-block t
  "Defines wheter a opening brace should follows a
if/else/while/do/for block line, if it has a non nil value then we
will have something like this:

        for (size_t i = 0; i < len; ++i) {
                                        ^
        }

instead of:

        for (size_t i = 0; i < len; ++i)
        {

        }"
  :type 'boolean
  :group 'c11)

(defcustom c11-mode-hook '()
  "C11 mode hook."
  :type 'hook
  :group 'c11)

(defcustom c11-label-offset 1
  "C11 label offset"
  :type 'integer
  :group 'c11)

(defvar c11-mode-map
  (let ((map (make-keymap)))
    (define-key map "\C-j" #'newline-and-indent)
    map)  "C11 mode map.")

(defconst c11-mode-syntax-table
  (with-syntax-table (make-syntax-table)
    (modify-syntax-entry ?< ".")
    (modify-syntax-entry ?> ".")
    (modify-syntax-entry ?| ".")
    (modify-syntax-entry ?& ".")
    (modify-syntax-entry ?+ ".")
    (modify-syntax-entry ?- ".")
    (modify-syntax-entry ?~ ".")
    (modify-syntax-entry ?= ".")
    (modify-syntax-entry ?\' "\"")
    (modify-syntax-entry ?* ". 23b")
    (modify-syntax-entry ?\\ "\\")
    (modify-syntax-entry ?/ ". 124")
    (modify-syntax-entry ?\n ">b")
    (modify-syntax-entry ?\r  ">b")
    (syntax-table))  "C syntax table.")

(defconst c11-lang-keywords '("break"
			      "case"
			      "continue"
			      "default"
			      "do"
			      "else"
			      "for"
			      "goto"
			      "if"
			      "return"
			      "switch"))


(defconst c11-storage-cls-specifier '("auto"
				      "extern"
				      "register"
				      "static"))

(defconst c11-qualifier-keywords '("_Atomic"
				   "_Complex"
				   "complex"
				   "const"
				   "_Imaginary"
				   "imaginary"
				   "inline"
				   "_Noreturn"
				   "Noreturn"
				   "noreturn"
				   "restrict"
				   "signed"
				   "_Thread_local"
				   "thread_local"
				   "typedef"
				   "volatile"
				   "unsigned"))

(defconst c11-operator-keywords '("_Alignas"
				  "alignas"
				  "_Alignof"
				  "alignof"
				  "_Generic"
				  "_Pragma"
				  "sizeof"
				  "_Static_assert"
				  "static_assert"))

(defconst c11-pp-directives-keywords '("#define"
				       "#if"
				       "#ifdef"
				       "#ifndef"
				       "#elif"
				       "#else"
				       "#endif"
				       "#error"
				       "#warning"
				       "#include"))

(defconst c11-builtin-types '("_Bool"
			      "bool"
			      "char"
			      "double"
			      "enum"
			      "float"
			      "int"
			      "long"
			      "short"
			      "struct"
			      "union"
			      "void"))

(defconst c11-bool-literal-keywords '("true" "false" "NULL" "NUL"))

(defvar c11-builtin-types-regex (concat "\\<" (regexp-opt c11-builtin-types t) "\\>"))

(defvar  c11-lang-keywords-regex (concat "\\<" (regexp-opt c11-lang-keywords t) "\\>"))


(defvar  c11-pp-directives-keywords-regex (regexp-opt c11-pp-directives-keywords t))

(defvar  c11-operator-keywords-regex (concat "\\<" (regexp-opt c11-operator-keywords t) "\\>"))

(defvar  c11-qualifier-keywords-regex (concat "\\<" (regexp-opt c11-qualifier-keywords t) "\\>"))
(defvar  c11-bool-literal-keywords-regex (concat "\\<" (regexp-opt c11-bool-literal-keywords t) "\\>"))

(defvar c11-font-lock-keywords nil "")

(setq c11-font-lock-keywords
      `((,c11-lang-keywords-regex . font-lock-keyword-face)
	(,c11-builtin-types-regex . font-lock-type-face)
	(,c11-pp-directives-keywords-regex . font-lock-preprocessor-face)
	(,c11-operator-keywords-regex . font-lock-keyword-face)
	(,c11-qualifier-keywords-regex . font-lock-keyword-face)
	(,c11-bool-literal-keywords-regex . font-lock-constant-face)))

;;;###autoload
(define-derived-mode c11-mode prog-mode "C11"
  "Major mode for editing C."
  :syntax-table c11-mode-syntax-table
  :group 'c11
  (use-local-map c11-mode-map)
  (setf major-mode 'c11-mode
	mode-name "C99/11")
  (setf font-lock-defaults '((c11-font-lock-keywords)))
  (font-lock-fontify-buffer)
  (run-mode-hooks c11-mode-hook))


(provide 'c11-mode)
