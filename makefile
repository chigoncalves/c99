
EMACS = gemacs

ELC := $(EMACS) -Q --batch -L . -f batch-byte-compile
RM := rm -rf

.PHONY: all
all: c11-mode.el
	@$(ELC) $<

.PHONY: clean
clean:
	@$(RM) *.elc
